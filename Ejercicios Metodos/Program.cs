﻿// See https://aka.ms/new-console-template for more information

bool result = true;
int numeroIntroducido = 0;
while (result)
{
  int opcion = 0;
  Console.WriteLine("Retos del Stalfos");
  Console.WriteLine("1 -> Introducir un valor entero impar comprendido entre 1 y 19");
  Console.WriteLine("2 -> Calcular la serie numérica 1 + 3 + 5 + ··· + n");
  Console.WriteLine("3 -> Calcular 1 * 3 * 5 * ··· * n");
  Console.WriteLine("4 -> Salir del programa");
  Console.WriteLine("Seleccione una opccion ->");
  opcion = Int32.Parse(Console.ReadLine()!);
  switch (opcion)
  {
    case 1:
    {
      bool numeroValido = false;
      while (!numeroValido)
      {
        int numero = 0;
        Console.WriteLine("Introduce el numero\n-> ");
        numero = Int32.Parse(Console.ReadLine()!);
        if ((numero >= 1 && numero <= 19) && (numero % 2 == 1))
        {
          Console.WriteLine($"Numero valido -> Numero Introducido {numero}");
          numeroValido = true;
          numeroIntroducido = numero;
        }
        else
        {
          Console.WriteLine("El numero no es par o no es entre 1 y 19");
        }
      }
    }
      break;
    case 2:
    {
      bool valido = false;
      while (!valido && numeroIntroducido != 0)
      {
        int impar = -1;
        int iteracion = 0;
        do
        {
          iteracion = iteracion + 1;
          impar = impar + 2;
          Console.WriteLine($"{impar},");
        } while (impar < numeroIntroducido);

        Console.WriteLine("\n");
        valido = true;
      }
    }
      break;
    case 3:
    {
      bool valido = false;
      while (!valido && numeroIntroducido != 0)
      {
        // int maximasIteraciones = 0;
        // Console.WriteLine("Introduce la cantidad maxima de iteraciones ");
        // maximasIteraciones = Int32.Parse(Console.ReadLine()!);
        // if (maximasIteraciones < 0)
        // {
        //   Console.WriteLine("El numero introducido no es positivo");
        //   continue;
        // }

        int impar = -1;     long resultadoProd = 1;
        do {
          impar = impar + 2;
          resultadoProd = resultadoProd * impar;
          Console.WriteLine(impar);
        } while (impar < numeroIntroducido);
        Console.WriteLine($"El resultado es: {resultadoProd}");
        Console.ReadLine();
        valido = true;
      }
    }
      break;
    case 4:
    {
      result = false;
    }
      break;
    default:
      Console.WriteLine("Opccion Invalida");
      break;
  }

  
}
Console.WriteLine("Adiossss :3");