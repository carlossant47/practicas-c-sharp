﻿// See https://aka.ms/new-console-template for more information

string ObtenerFraccion(double x)
{
  if (x < 0){
    return "-" + ObtenerFraccion(-x);
  }
  double tolerance = 1.0E-6;
  double h1=1; double h2=0;
  double k1=0; double k2=1;
  double b = x;
  do {
    double a = Math.Floor(b);
    double aux = h1; h1 = a*h1+h2; h2 = aux;
    aux = k1; k1 = a*k1+k2; k2 = aux;
    b = 1/(b-a);
  } while (Math.Abs(x-h1/k1) > x*tolerance);

  return h1+"/"+k1;
}

bool result = true;
int numeroIntroducido = 0;
while (result)
{
  int opcion = 0;
  Console.WriteLine("Retos del Stalfos");
  Console.WriteLine("1 ->  Introducir un numero entero positivo ese numero será el numero de veces que hará la sucesión el programa debe validar que en efecto es un numero entero positivo");
  Console.WriteLine("2 -> Calcular 1/2 + 1/2^2 + 1/2^3 + ... + 1/2^n");
  Console.WriteLine("3 -> Salir");
  Console.WriteLine("Seleccione una opccion ->");
  opcion = Int32.Parse(Console.ReadLine()!);
  switch (opcion)
  {
    case 1:
    {
      int numero = 0;
      Console.WriteLine("Introduce el numero\n-> ");
      numero = Int32.Parse(Console.ReadLine()!);
      if (numero <= 0)
      {
        Console.WriteLine("Se requiere un numero positivo");
        continue;
      }
      Console.Clear();
      numeroIntroducido = numero;
    } break;
    case 2:
    {
      bool valido = false;
      while (!valido && numeroIntroducido > 0)
      {
        double resultado = 0;
        for (int iterator = 1; iterator <= numeroIntroducido; iterator++)
        {
          resultado += (1 / Math.Pow(2, iterator));
        }
        Console.WriteLine($"El resultado es: {resultado} o {ObtenerFraccion(resultado)}");
        Console.ReadLine();
        Console.Clear();
        valido = true;
        
      }
    } break;
    case 3:
      result = false;
      break;
    default:
      Console.WriteLine("Opccion Invalida");
      break;
  }
}